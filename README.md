# nd-front

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Rutas de peticion

TODO: Agregar en un archivo .env las configuraciones locales
La ruta para las peticiones se cambia en
-src/store/index.js

Tambien se debe cambiar en cada modulo

- src/modules/collections.modules.js
- src/modules/location.modules.js
- src/modules/projects.modules.js
- src/modules/users.modules.js
