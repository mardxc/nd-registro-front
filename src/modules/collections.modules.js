const url = "https://nd-registro.herokuapp.com";

export default {
  namespaced: true,
  state: {
    statuses: [
      {
        _id: "1",
        description: "None",
      },
    ],
    statusSave: {
      _id: "",
      description: "",
    },
    phaseSave: {
      _id: "",
      description: "",
    },
    activitySave: {
      _id: "",
      description: "",
    },
    delaySave: {
      _id: "",
      description: "",
    },
    projectTypeSave: {
      _id: "",
      description: "",
    },
    estimatedModelSave: {
      _id: "",
      description: "",
    },
    lifecycleSave: {
      _id: "",
      description: "",
    },
    phases: [
      {
        _id: "1",
        description: "None",
      },
    ],
    activites: [
      {
        _id: "1",
        description: "None",
      },
    ],
    delayes: [
      {
        _id: "1",
        description: "None",
      },
    ],
    projectTypes: [
      {
        _id: "1",
        description: "None",
      },
    ],
    lifecycles: [
      {
        _id: "1",
        description: "None",
      },
    ],
    estimatedModel: [
      {
        _id: "1",
        description: "None",
      },
    ],
  },
  mutations: {
    setEditStatus(state, edit) {
      state.statusSave._id = edit._id;
      state.statusSave.description = edit.description;
    },
    loadStatuses(state, dataLoad) {
      state.statusSave._id = "";
      state.statusSave.description = "";
      state.statuses = dataLoad;
    },
    setEditPhase(state, edit) {
      state.phaseSave._id = edit._id;
      state.phaseSave.description = edit.description;
    },
    setEditActivity(state, edit) {
      state.activitySave._id = edit._id;
      state.activitySave.description = edit.description;
    },
    setEditDelay(state, edit) {
      state.delaySave._id = edit._id;
      state.delaySave.description = edit.description;
    },
    setEditProjectType(state, edit) {
      state.projectTypeSave._id = edit._id;
      state.projectTypeSave.description = edit.description;
    },
    setEditLifecycle(state, edit) {
      state.lifecycleSave._id = edit._id;
      state.lifecycleSave.description = edit.description;
    },
    setEditEstimatedModel(state, edit) {
      state.estimatedModelSave._id = edit._id;
      state.estimatedModelSave.description = edit.description;
    },
    loadEstimatedModel(state, dataLoad) {
      state.estimatedModelSave._id = "";
      state.estimatedModelSave.description = "";
      state.estimatedModel = dataLoad;
    },
    loadPhases(state, dataLoad) {
      state.phaseSave._id = "";
      state.phaseSave.description = "";
      state.phases = dataLoad;
    },
    loadActivities(state, dataLoad) {
      state.activitySave._id = "";
      state.activitySave.description = "";
      state.activites = dataLoad;
    },
    loadDelayes(state, dataLoad) {
      state.delaySave._id = "";
      state.delaySave.description = "";
      state.delayes = dataLoad;
    },
    loadProjectType(state, dataLoad) {
      state.projectTypeSave._id = "";
      state.projectTypeSave.description = "";
      state.projectTypes = dataLoad;
    },
    loadLifecycle(state, dataLoad) {
      state.lifecycleSave._id = "";
      state.lifecycleSave.description = "";
      state.lifecycles = dataLoad;
    },
  },
  actions: {
    async uptadeStatus({ commit, dispatch }, statusSave) {
      try {
        const response = await fetch(`${url}/api/status/${statusSave._id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(statusSave),
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getStatus");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async editStatus({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/status/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setEditStatus", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async saveStatus({ dispatch }, status) {
      try {
        const response = await fetch(`${url}/api/status`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(status),
        });

        const result = await response.json();

        if (response.status == 201) {
          swal(`${result.message}`, ``, "success");
          dispatch("getStatus");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async deleteStatus({ dispatch }, id) {
      try {
        const response = await fetch(`${url}/api/status/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getStatus");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getStatus({ commit }) {
      try {
        const response = await fetch(`${url}/api/status`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadStatuses", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async updatePhase({ commit, dispatch }, phaseSave) {
      try {
        const response = await fetch(`${url}/api/phase/${phaseSave._id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(phaseSave),
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getPhases");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async editPhase({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/phase/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setEditPhase", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async savePhase({ dispatch }, status) {
      try {
        const response = await fetch(`${url}/api/phase`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(status),
        });

        const result = await response.json();

        if (response.status == 201) {
          swal(`${result.message}`, ``, "success");
          dispatch("getPhases");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async deletePhases({ dispatch }, id) {
      try {
        const response = await fetch(`${url}/api/phase/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getPhases");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getPhases({ commit }) {
      try {
        const response = await fetch(`${url}/api/phase`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadPhases", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async updateActivity({ commit, dispatch }, activitySave) {
      try {
        const response = await fetch(
          `${url}/api/activity/${activitySave._id}`,
          {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
              "x-access-token": localStorage.getItem("token"),
            },
            body: JSON.stringify(activitySave),
          }
        );

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getActivities");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async editActivity({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/activity/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setEditActivity", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async saveActivity({ dispatch }, status) {
      try {
        const response = await fetch(`${url}/api/activity`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(status),
        });

        const result = await response.json();

        if (response.status == 201) {
          swal(`${result.message}`, ``, "success");
          dispatch("getActivities");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async deleteActivities({ dispatch }, id) {
      try {
        const response = await fetch(`${url}/api/activity/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getActivities");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getActivities({ commit }) {
      try {
        const response = await fetch(`${url}/api/activity`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadActivities", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async updateDelay({ commit, dispatch }, delaySave) {
      try {
        const response = await fetch(`${url}/api/delay/${delaySave._id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(delaySave),
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getDelayes");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async editDelay({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/delay/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setEditDelay", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async saveDelay({ dispatch }, status) {
      try {
        const response = await fetch(`${url}/api/delay`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(status),
        });

        const result = await response.json();

        if (response.status == 201) {
          swal(`${result.message}`, ``, "success");
          dispatch("getDelayes");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async deleteDelayes({ dispatch }, id) {
      try {
        const response = await fetch(`${url}/api/delay/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getDelayes");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getDelayes({ commit }) {
      try {
        const response = await fetch(`${url}/api/delay`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadDelayes", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getProjectType({ commit }) {
      try {
        const response = await fetch(`${url}/api/type`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadProjectType", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async editProjectType({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/type/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setEditProjectType", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async saveProjectType({ dispatch }, status) {
      try {
        const response = await fetch(`${url}/api/type`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(status),
        });

        const result = await response.json();

        if (response.status == 201) {
          swal(`${result.message}`, ``, "success");
          dispatch("getProjectType");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async updateProjectType({ commit, dispatch }, obj) {
      try {
        const response = await fetch(`${url}/api/type/${obj._id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(obj),
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getProjectType");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async deleteProjectType({ dispatch }, id) {
      try {
        const response = await fetch(`${url}/api/type/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getProjectType");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getEstimatedModel({ commit }) {
      try {
        const response = await fetch(`${url}/api/estimation_model`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadEstimatedModel", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },

    async editEstimatedModel({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/estimation_model/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setEditEstimatedModel", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },

    async saveEstimatedModel({ dispatch }, status) {
      try {
        const response = await fetch(`${url}/api/estimation_model`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(status),
        });

        const result = await response.json();

        if (response.status == 201) {
          swal(`${result.message}`, ``, "success");
          dispatch("getEstimatedModel");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async updateEstimatedModel({ commit, dispatch }, obj) {
      try {
        const response = await fetch(`${url}/api/estimation_model/${obj._id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(obj),
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getEstimatedModel");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async deleteEstimatedModel({ dispatch }, id) {
      try {
        const response = await fetch(`${url}/api/estimation_model/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getEstimatedModel");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getLifecicle({ commit }) {
      try {
        const response = await fetch(`${url}/api/lifecycle`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadLifecycle", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async editLifecycle({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/lifecycle/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setEditLifecycle", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async saveLifecycle({ dispatch }, status) {
      try {
        const response = await fetch(`${url}/api/lifecycle`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(status),
        });

        const result = await response.json();

        if (response.status == 201) {
          swal(`${result.message}`, ``, "success");
          dispatch("getLifecicle");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async updateLifecycle({ commit, dispatch }, obj) {
      try {
        const response = await fetch(`${url}/api/lifecycle/${obj._id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(obj),
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getLifecicle");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async deleteLifecycle({ dispatch }, id) {
      try {
        const response = await fetch(`${url}/api/lifecycle/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getLifecicle");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
  },
  getters: {},
};
