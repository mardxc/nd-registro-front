const url = "https://nd-registro.herokuapp.com";

export default {
  namespaced: true,
  state: {
    locations: [{}],
    loc: {},
  },
  mutations: {
    loadLocations(state, userLoad) {
      state.locations = userLoad;
    },
  },
  actions: {
    async createGeolocation({ commit, dispatch }, loc) {
      try {
        const response = await fetch(`${url}/api/location/`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(loc),
        });

        const result = await response.json();
        if (response.status == 201) {
          swal(`${result.message}`, ``, "success");
          dispatch("getLocations");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getLocations({ commit }) {
      try {
        const response = await fetch(`${url}/api/location/`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadLocations", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
  },
  getters: {},
};
