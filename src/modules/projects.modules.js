const url = "https://nd-registro.herokuapp.com";

export default {
  namespaced: true,
  state: {
    projects: [
      {
        title: "",
        description: "",
        status: {
          _id: "",
          description: "",
        },
        user: {
          _id_: "",
          name: "",
        },
      },
    ],
    saveProject: {
      _id: "",
      code: "",
      name: "",
      type: "",
      lifecycle: "",
      start: "",
      end: "",
      duration: "",
      coverage: "",
      salesHours: "",
      estimateHours: "",
      estimateModel: "",
      cosmicSize: "",
      productivity: "",
      percentDefects: "",
      percentDefectsClient: "",
      salesAmount: "",
      staffCost: "",
      projectCost: "",
      projectBudget: "",
      profit: "",
      percentProfit: "",
      salesRate: "",
      client: "",
      responsable: "",
      projectManager: "",
      leader: "",
      collaborators: [],
    },
    users: [
      {
        _id_: "1",
        name: "D",
      },
    ],
    assignUsers: [],
    lifecycle: [
      {
        _id: "",
        description: "",
      },
    ],
    types: [
      {
        _id: "",
        description: "",
      },
    ],
    estimateModel: [
      {
        _id: "",
        description: "",
      },
    ],
    projectTasks: [
      {
        project: { name: "" },
        user: { name: "", lastname: "" },
        status: { description: "" },
        phase: { description: "" },
      },
    ],
    refreshTasks: {
      id: "",
      start: "",
      end: "",
    },
  },
  mutations: {
    loadProjects(state, dataLoad) {
      state.saveProject._id = "";
      state.saveProject.code = "";
      state.saveProject.name = "";
      state.saveProject.type = "";
      state.saveProject.lifecycle = "";
      state.saveProject.start = "";
      state.saveProject.end = "";
      state.saveProject.duration = "";
      state.saveProject.coverage = "";
      state.saveProject.salesHours = "";
      state.saveProject.estimateHours = "";
      state.saveProject.estimateModel = "";
      state.saveProject.cosmicSize = "";
      state.saveProject.productivity = "";
      state.saveProject.percentDefects = "";
      state.saveProject.percentDefectsClient = "";
      state.saveProject.salesAmount = "";
      state.saveProject.staffCost = "";
      state.saveProject.projectCost = "";
      state.saveProject.projectBudget = "";
      state.saveProject.profit = "";
      state.saveProject.percentProfit = "";
      state.saveProject.salesRate = "";
      state.saveProject.client = "";
      state.saveProject.responsable = "";
      state.saveProject.projectManager = "";
      state.saveProject.leader = "";
      state.saveProject.collaborators = [];
      state.projects = dataLoad;
    },
    setEditProject(state, edit) {
      state.saveProject._id = edit._id;
      state.saveProject.code = edit.code;
      state.saveProject.name = edit.name;
      state.saveProject.type = edit.type;
      state.saveProject.lifecycle = edit.lifecycle;
      state.saveProject.start = edit.start.substring(0, 10);
      state.saveProject.end = edit.end.substring(0, 10);
      state.saveProject.duration = edit.duration;
      state.saveProject.coverage = edit.coverage;
      state.saveProject.salesHours = edit.salesHours.$numberDecimal;
      state.saveProject.estimateHours = edit.estimateHours.$numberDecimal;
      state.saveProject.estimateModel = edit.estimateModel;
      state.saveProject.cosmicSize = edit.cosmicSize.$numberDecimal;
      state.saveProject.productivity = edit.productivity.$numberDecimal;
      state.saveProject.percentDefects = edit.percentDefects.$numberDecimal;
      state.saveProject.percentDefectsClient =
        edit.percentDefectsClient.$numberDecimal;
      state.saveProject.salesAmount = edit.salesAmount.$numberDecimal;
      state.saveProject.staffCost = edit.staffCost.$numberDecimal;
      state.saveProject.projectCost = edit.projectCost.$numberDecimal;
      state.saveProject.projectBudget = edit.projectBudget.$numberDecimal;
      state.saveProject.profit = edit.profit.$numberDecimal;
      state.saveProject.percentProfit = edit.percentProfit.$numberDecimal;
      state.saveProject.salesRate = edit.salesRate.$numberDecimal;
      state.saveProject.client = edit.client;
      state.saveProject.responsable = edit.responsable;
      state.saveProject.projectManager = edit.projectManager;
      state.saveProject.leader = edit.leader;
      state.saveProject.collaborators = edit.collaborators;
    },
    loadUsers(state, dataLoad) {
      state.users = dataLoad;
    },
    loadLifecycle(state, dataLoad) {
      state.lifecycle = dataLoad;
    },
    loadType(state, dataLoad) {
      state.types = dataLoad;
    },
    loadEstimateModel(state, dataLoad) {
      state.estimateModel = dataLoad;
    },
    loadProjectTasks(state, dataLoad) {
      state.projectTasks = dataLoad;
    },
    setUserId(state, id) {
      state.refreshTasks.id = id;
    },
  },
  actions: {
    async deleteProject({ dispatch }, id) {
      try {
        const response = await fetch(`${url}/api/project/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getProjects");
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async createProject({ commit, dispatch }, project) {
      try {
        const response = await fetch(`${url}/api/project/`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(project),
        });

        const result = await response.json();
        if (response.status == 201) {
          swal(`${result.message}`, ``, "success");
          dispatch("getProjects");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getProjects({ commit }) {
      try {
        const response = await fetch(`${url}/api/project/`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadProjects", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getUsers({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/users`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadUsers", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getLifecycle({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/lifecycle`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadLifecycle", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getType({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/type`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadType", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getEstimateModel({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/estimation_model`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadEstimateModel", result.data);
        } else {
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async editProject({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/project/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setEditProject", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async updateProject({ commit, dispatch }, project) {
      try {
        const response = await fetch(`${url}/api/project/${project._id}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(project),
        });

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getProjects");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getProjectTasks({ commit }, data) {
      try {
        const response = await fetch(`${url}/api/task/project/${data.id}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(data),
        });
        commit("setUserId", data.id);

        const result = await response.json();

        if (response.status == 200) {
          commit("loadProjectTasks", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
  },
  getters: {},
};
