const url = "https://nd-registro.herokuapp.com";

export default {
  namespaced: true,
  state: {
    users: [{}],
    userMod: {
      name: "",
      lastname: "",
      birthday: "",
      email: "",
      username: "",
    },
    refreshTasks: {
      id: "",
      start: "",
      end: "",
    },
    userTasks: [
      {
        project: { name: "" },
        user: { name: "", lastname: "" },
        status: { description: "" },
        phase: { description: "" },
      },
    ],
    userLocs: [{}],
    userLoc: {
      userId: "",
      locations: "",
    },
    userId: "",
  },
  mutations: {
    loadUsers(state, userLoad) {
      state.users = userLoad;
    },
    loadUserTasks(state, userLoad) {
      state.userTasks = userLoad;
    },
    setUserId(state, id) {
      state.refreshTasks.id = id;
    },
    loadUserLocs(state, userLoad) {
      state.userLocs = userLoad.location;
      state.userLoc.userId = userLoad.userId;
      state.userId = userLoad.userId;
    },
    setUserMod(state, userLoad) {
      state.userMod._id = userLoad._id;
      state.userMod.name = userLoad.name;
      state.userMod.lastname = userLoad.lastname;
      state.userMod.birthday = userLoad.birthday;
      state.userMod.email = userLoad.email;
      state.userMod.username = userLoad.username;
    },
    closeModalUser(state) {
      state.usersVisible = false;
    },
  },
  actions: {
    async getUsers({ commit }) {
      try {
        const response = await fetch(`${url}/api/users/`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadUsers", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getUserMod({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/users/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setUserMod", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async saveUserMod({ commit, dispatch }, userMod) {
      try {
        const response = await fetch(
          `${url}/api/users/update-user/${userMod._id}`,
          {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
              "x-access-token": localStorage.getItem("token"),
            },
            body: JSON.stringify(userMod),
          }
        );

        const result = await response.json();

        if (response.status == 200) {
          swal(`${result.message}`, ``, "success");
          dispatch("getUsers");
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getUserTasks({ commit }, task) {
      try {
        const response = await fetch(`${url}/api/task/user/${task.id}`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
          body: JSON.stringify(task),
        });
        commit("setUserId", task.id);

        const result = await response.json();

        if (response.status == 200) {
          commit("loadUserTasks", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async getUserLocs({ commit }, id) {
      try {
        const response = await fetch(`${url}/api/location/${id}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("loadUserLocs", result.data);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async assingLocation({ commit, dispatch }, userLoc) {
      try {
        const response = await fetch(`${url}/api/location/assing-location`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },

          body: JSON.stringify(userLoc),
        });

        const result = await response.json();

        if (response.status == 200) {
          dispatch("getUserLocs", userLoc.userId);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async unassingLocation({ commit, dispatch, state }, userLoc) {
      try {
        const response = await fetch(`${url}/api/location/unassing-location`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-access-token": localStorage.getItem("token"),
          },

          body: JSON.stringify({
            locations: userLoc,
            userId: state.userId,
          }),
        });

        const result = await response.json();

        if (response.status == 200) {
          dispatch("getUserLocs", userLoc.userId);
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
  },
  getters: {},
};
