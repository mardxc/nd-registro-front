import { createStore } from "vuex";
import router from "../router";
import swal from "sweetalert";

import usersModule from "@/modules/users.modules";
import locationsModule from "@/modules/locations.modules";
import projectsModule from "@/modules/projects.modules";
import collectionsModule from "@/modules/collections.modules";

const url = "https://nd-registro.herokuapp.com";

export default createStore({
  state: {
    token: null,
    showLoading: false,
    datatableOptions: {
      dom: "Bfrtip",
      destroy: true,
      buttons: [
        {
          extend: "excelHtml5",
          text: "Guardar Excel",
          exportOptions: {
            modifier: {
              page: "current",
            },
          },
        },
      ],
      language: {
        url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
      },
    },
  },
  mutations: {
    startLoading: (state) => (state.showLoading = true),
    endLoading: (state) => (state.showLoading = false),
    setToken(state, payload) {
      state.token = payload;
    },
  },
  actions: {
    async register({ commit }, user) {
      try {
        //commit('startLoading');
        const response = await fetch(`${url}/api/auth/signup`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(user),
        });

        const result = await response.json();

        if (response.status == 201) {
          commit("setToken", result.token);
          localStorage.setItem("token", result.token);
          swal(`${result.message}`, ``, "success").then((value) => {
            if (value) {
              router.push("/");
            }
          });
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    async login({ commit }, user) {
      try {
        //commit('startLoading');
        const response = await fetch(`${url}/api/auth/signin`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(user),
        });

        const result = await response.json();

        if (response.status == 200) {
          commit("setToken", result.token);
          localStorage.setItem("token", result.token);
          swal(`${result.message}`, ``, "success").then((value) => {
            if (value) {
              router.push("/");
            }
          });
        } else {
          //commit('endLoading');
          swal(`${result.message}`, ``, "error");
        }
      } catch (error) {
        //commit('endLoading');
        swal(`${error.message}`, ``, "error");
        console.log("error: ", error);
      }
    },
    obtenerToken({ commit }) {
      if (localStorage.getItem("token")) {
        commit("setToken", localStorage.getItem("token"));
      } else {
        commit("setToken", null);
      }
    },
    cerrarSesion({ commit }) {
      commit("setToken", null);
      localStorage.removeItem("token");
      router.push("/");
    },
  },
  modules: {
    usersModule,
    locationsModule,
    projectsModule,
    collectionsModule,
  },
});
